﻿using Game.Constant;
using Game.Piece;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkPieceHandler : MonoBehaviour
{

    Falldown falldownComponent;

    // Use this for initialization
    void OnEnable()
    {
        WaitFor2TeamConnected.OnEnoughClientConnected += OnEnoughClientConnected;
        WaitFor2TeamConnected.OnNotEnoughClientConnected += OnNotEnoughClientConnected;
    }

    void OnDisable()
    {
        WaitFor2TeamConnected.OnEnoughClientConnected -= OnEnoughClientConnected;
        WaitFor2TeamConnected.OnNotEnoughClientConnected -= OnNotEnoughClientConnected;
    }
    void OnEnoughClientConnected()
    {
        SetLocalManager();
        falldownComponent.OnPieceStopFalling += OnPieceStopFalling;
    }

    void OnNotEnoughClientConnected()
    {
        falldownComponent = null;
        falldownComponent.OnPieceStopFalling -= OnPieceStopFalling;
    }

    void SetLocalManager()
    {
        Debug.Log("Start !!!!");
        GameObject[] managerList = GameObject.FindGameObjectsWithTag(TagConstant.PLAYER);
        foreach (GameObject go in managerList)
        {
            bool hasAuthority = go.GetComponent<NetworkIdentity>().hasAuthority;
            if (hasAuthority)
            {
                falldownComponent = go.GetComponent<Falldown>();
                break;
            }
        }
    }

    void OnPieceStopFalling(GameObject piece)
    {
        piece.GetComponent<NetworkTransform>().sendInterval = 0;
    }

}
