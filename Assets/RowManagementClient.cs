﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Piece.Network
{
    public class RowManagementClient : MonoBehaviour
    {
        public const int MAX_BLOCK_COUNT = 10;

        public FalldownClient falldownComponent;

        int blockCount;
        IList<Collider> currentGoPartOnRow = new List<Collider>();


        void OnEnable()
        {
            ClientMessageReceptionHandler.OnStopFall += OnStopFall;
        }

        void OnTriggerEnter(Collider other)
        {
            blockCount++;
            currentGoPartOnRow.Add(other);
        }

        void OnTriggerExit(Collider other)
        {
            blockCount--;
            currentGoPartOnRow.Remove(other);
        }

        void OnDisable()
        {
            ClientMessageReceptionHandler.OnStopFall -= OnStopFall;
        }

        void OnStopFall()
        {
            if (blockCount < MAX_BLOCK_COUNT)
                return;

            for (int i = 0; i < currentGoPartOnRow.Count; i++)
                Destroy(currentGoPartOnRow[i].gameObject);
            
            ResetRow();
            NotifyRemoveLine(falldownComponent, gameObject);
        }

        void ResetRow()
        {
            currentGoPartOnRow = new List<Collider>();
            blockCount = 0;
        }

        public static event LineRemoved OnLineRemoved;
        public delegate void LineRemoved(FalldownClient manager, GameObject line);
        public static void NotifyRemoveLine(FalldownClient manager, GameObject line)
        {
            if (OnLineRemoved == null)
                return;

            OnLineRemoved(manager, line);
        }
    }
}