﻿using System.Net;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Game.Constant;

public class SceneConnectionManager : MonoBehaviour
{
    public Canvas MainCanvas;
    public Canvas HostCanvas;
    public Canvas ClientCanvas;
    public InputField ServerIpInfo;
    public InputField ClientChosenIp;
    public InputField ClientChosenPort;
    public InputField HostChosenPort;

    bool hasStarted;

    void Start()
    {
        ServerIpInfo.text = LocalIPAddress();
    }

    public void ChooseHost()
    {
        DisplayCanvas(HostCanvas);
    }

    public void ChooseClient()
    {
        DisplayCanvas(ClientCanvas);
    }

    public void GoBack()
    {
        DisplayCanvas(MainCanvas);
    }

    public void StartHost()
    {
        int port = int.Parse(HostChosenPort.text);

        NetworkUtils.StartHost(port, GoToGameWhenConnected);
        InvokeRepeating("WaitingForConnection", 0, 1f);
    }

    public void StartClient()
    {
        string ip = ClientChosenIp.text;
        int port = int.Parse(ClientChosenPort.text);

        NetworkUtils.StartClient(ip, port, GoToGameWhenConnected);
        InvokeRepeating("WaitingForConnection", 0, 1f);
    }

    void WaitingForConnection()
    {
        if (!hasStarted)
        {
            Debug.Log(".");
            return;
        }
        Debug.Log("Waiting finished");
        CancelInvoke("WaitingForConnection");
    }

    void DisplayCanvas(Canvas c)
    {
        MainCanvas.gameObject.SetActive(false);
        HostCanvas.gameObject.SetActive(false);
        ClientCanvas.gameObject.SetActive(false);
        c.gameObject.SetActive(true);
    }

    string LocalIPAddress()
    {
        IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
                return ip.ToString();
        }
        return "";
    }

    void GoToGameWhenConnected(NetworkMessage netMsg)
    {
        hasStarted = true;
        SceneManager.LoadScene(SceneConstant.MULTIPLAYER_SCENE);
    }
}
