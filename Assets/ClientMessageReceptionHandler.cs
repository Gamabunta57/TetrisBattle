﻿using System;
using Game.Constant;
using Game.Network.Message;
using Game.Piece.Network;
using UnityEngine;
using UnityEngine.Networking;

public class ClientMessageReceptionHandler : MonoBehaviour
{

    public GameObject LocalPlayerArea;
    public GameObject NetworkPlayerArea;

    NetworkClient client;
    FalldownClient fcc;
    MovingLeftRightClient mlrc;
    RotationClient rc;
    NewPieceGenerationClient npgc;
    RowRemoveCounterClient rrcc;


    void Awake()
    {
        client = NetworkUtils.GetClient();
        InitNetworkComponent();
        RegisterHandler();
    }

    void RegisterHandler()
    {
        client.RegisterHandler(MessageId.START_GAME, OnStartGame);
        client.RegisterHandler(MessageId.PIECE_FALL, OnPieceFall);
        client.RegisterHandler(MessageId.PIECE_LEFT_RIGHT, OnPieceLeftRight);
        client.RegisterHandler(MessageId.PIECE_ROTATE, OnPieceRotate);
        client.RegisterHandler(MessageId.PIECE_SPAWN, OnPieceSpawn);
        client.RegisterHandler(MessageId.PIECE_STOP_FALL, OnPieceStopFall);
    }

    void InitNetworkComponent()
    {
        fcc = NetworkPlayerArea.GetComponentInChildren<FalldownClient>();
        mlrc = NetworkPlayerArea.GetComponentInChildren<MovingLeftRightClient>();
        rc = NetworkPlayerArea.GetComponentInChildren<RotationClient>();
        npgc = NetworkPlayerArea.GetComponentInChildren<NewPieceGenerationClient>();
        rrcc = NetworkPlayerArea.GetComponentInChildren<RowRemoveCounterClient>();
    }

    void OnStartGame(NetworkMessage netMsg)
    {
        LocalPlayerArea.SetActive(true);
        NetworkPlayerArea.SetActive(true);
    }

    void OnPieceFall(NetworkMessage netMsg)
    {
        Debug.Log("Receive piece fall");
        fcc.MakePieceFall();
    }

    void OnPieceLeftRight(NetworkMessage netMsg)
    {
        PieceMoveMessage msg = netMsg.ReadMessage<PieceMoveMessage>();
        Debug.Log("Receive moving left/right : " + (msg.isDirectionLeft ? "LEFT" : "RIGHT"));
        mlrc.MakePieceMoveRightOrLeft(msg.isDirectionLeft);
    }

    void OnPieceRotate(NetworkMessage netMsg)
    {
        PieceRotateMessage msg = netMsg.ReadMessage<PieceRotateMessage>();
        Debug.Log("Receive rotate left/right : " + (msg.isRotationLeft ? "LEFT" : "RIGHT"));
        rc.MakePieceRotate(msg.isRotationLeft);
    }

    void OnPieceSpawn(NetworkMessage netMsg)
    {
        PieceSpawnMessage msg = netMsg.ReadMessage<PieceSpawnMessage>();
        Debug.Log("Receive piece spawn id : " + msg.PieceId);
        npgc.InstantiateNewPiece(msg.PieceId);
    }

    void OnPieceStopFall(NetworkMessage netMsg)
    {
        PieceStopFallingMessage mdg = netMsg.ReadMessage<PieceStopFallingMessage>();
        Debug.Log("Receive Stop Fall");
        NotifyStopFall();
    }

    public static event StopFall OnStopFall;
    public delegate void StopFall();
    static void NotifyStopFall(){
        if (null == OnStopFall)
            return;

        OnStopFall();
    }
}
