﻿using UnityEngine;
using UnityEngine.Networking;
using Game.Utils;

public class StartMultiPlayerGame : NetworkBehaviour
{
    [SyncVar]
    int seed;

    void OnEnable()
    {
        WaitFor2TeamConnected.OnEnoughClientConnected += OnEnoughClientConnected;
    }

    void Start()
    {
        if (!isServer)
        {
            return;
        }
        seed = RandomUtils.InitRandom();
        Debug.Log("Start server : " + seed.ToString());
    }

    void OnDisable()
    {
        WaitFor2TeamConnected.OnEnoughClientConnected -= OnEnoughClientConnected;
    }

    void OnEnoughClientConnected()
    {
        RpcStartGame();
    }

    [ClientRpc]
    void RpcStartGame()
    {
        if (!isClient)
        {
            return;
        }
        RandomUtils.InitSeedWith(seed);
        Debug.Log("Start : " + seed.ToString());
    }
}
