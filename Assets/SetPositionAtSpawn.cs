﻿using UnityEngine;
using UnityEngine.Networking;

public class SetPositionAtSpawn : NetworkBehaviour {

    public Transform spawnLocal;
    public Transform spawnOther;

    void Awake(){
        if(isLocalPlayer){
            return;
        }


        gameObject.transform.position = spawnOther.position;
    }

    public override void OnStartLocalPlayer()
    {
        gameObject.transform.position = spawnLocal.position;
    }
}
