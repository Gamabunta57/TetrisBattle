﻿using Game.Constant;
using Game.Piece;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public class GameOverLauncher : MonoBehaviour
    {
        NewPieceGeneration newPieceGenerationComponent;

        int playerCount;
        int gameOverCount;

        void OnEnable()
        {
            GameObject[] goList = GameObject.FindGameObjectsWithTag(TagConstant.MAIN_GAME_MANAGER);
            playerCount = goList.Length;
            for (int i = 0; i < goList.Length; i++)
            {
                goList[i].GetComponent<NewPieceGeneration>().OnEndOfGame += OnEndOfGame;
            }
        }

        void OnDisable()
        {

            GameObject[] goList = GameObject.FindGameObjectsWithTag(TagConstant.MAIN_GAME_MANAGER);
            for (int i = 0; i < goList.Length; i++)
            {
                goList[i].GetComponent<NewPieceGeneration>().OnEndOfGame += OnEndOfGame;
            }
        }

        void OnEndOfGame(GameObject manager)
        {
            gameOverCount++;
            if(gameOverCount >= playerCount){
                SceneManager.LoadScene(SceneConstant.GAME_OVER, LoadSceneMode.Additive);
            }
        }
    }
}