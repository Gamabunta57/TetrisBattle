﻿using Game.Piece;
using UnityEngine;
using Game.Constant;

namespace Game
{

    public class StopOnGameOver : MonoBehaviour
    {
        NewPieceGeneration newPieceGenerationComponent;
        int playerCount;
        int gameOverCount;

        void Awake()
        {
            newPieceGenerationComponent = GetComponent<NewPieceGeneration>();
        }

        void OnEnable()
        {
            GameObject[] goList = GameObject.FindGameObjectsWithTag(TagConstant.MAIN_GAME_MANAGER);
            playerCount = goList.Length;
            for (int i = 0; i < goList.Length; i++){
                NewPieceGeneration component = goList[i].GetComponent<NewPieceGeneration>();
                component.OnEndOfGame += OnEndOfGame;
            }
        }

        void OnDisable()
        {

            GameObject[] goList = GameObject.FindGameObjectsWithTag(TagConstant.MAIN_GAME_MANAGER);
            for (int i = 0; i < goList.Length; i++)
            {
                NewPieceGeneration component = goList[i].GetComponent<NewPieceGeneration>();
                component.OnEndOfGame -= OnEndOfGame;
            }
        }

        void OnEndOfGame(GameObject manager)
        {
            manager.SetActive(false);
            gameOverCount++;

            if(gameOverCount == playerCount){
                gameObject.SetActive(false);
            }
        }
    }
}