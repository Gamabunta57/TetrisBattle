﻿using Game.Piece;
using UnityEngine;

namespace Game
{
    public class BackActionManager : MonoBehaviour
    {

        public AudioClip feltSound;
        public AudioClip removedRow;
        public AudioClip movingSound;
        public AudioClip levelUpSound;
        public AudioClip gameOverSound;

        public AudioSource player;

        Falldown falldownComponent;
        Rotation rotationComponent;
        MovingLeftRight movingLeftRightComponent;
        LevelHandler levelHandlerComponent;
        NewPieceGeneration newPieceGenerationComponent;

        void Awake()
        {
            falldownComponent = GetComponent<Falldown>();
            rotationComponent = GetComponent<Rotation>();
            movingLeftRightComponent = GetComponent<MovingLeftRight>();
            levelHandlerComponent = GetComponent<LevelHandler>();
            newPieceGenerationComponent = GetComponent<NewPieceGeneration>();
        }

        void OnEnable()
        {
            falldownComponent.OnPieceStopFalling += OnPieceStopFalling;
            falldownComponent.OnPieceFalling += PlayMovingSound;
            rotationComponent.OnPieceRotate += PlayMovingSound;
            movingLeftRightComponent.OnPieceMovingLeftRight += PlayMovingSound;
            RowManagement.OnLineRemoved += PlayRowRemoved;
            levelHandlerComponent.OnLevelChange += PlayLevelUp;
            newPieceGenerationComponent.OnEndOfGame += PlayGameOver;
        }

        void OnDisable()
        {
            falldownComponent.OnPieceStopFalling -= OnPieceStopFalling;
            falldownComponent.OnPieceFalling -= PlayMovingSound;
            rotationComponent.OnPieceRotate -= PlayMovingSound;
            movingLeftRightComponent.OnPieceMovingLeftRight -= PlayMovingSound;
            RowManagement.OnLineRemoved -= PlayRowRemoved;
            levelHandlerComponent.OnLevelChange -= PlayLevelUp;
            newPieceGenerationComponent.OnEndOfGame -= PlayGameOver;
        }

        void OnPieceStopFalling(GameObject piece)
        {
            DoVibrate();
            if (null != feltSound)
            {
                player.PlayOneShot(feltSound);
            }
        }

        void PlayMovingSound(bool isMovingToLeft)
        {
            PlayMovingSound();
        }

        void PlayMovingSound()
        {
            if (null != movingSound)
            {
                player.PlayOneShot(movingSound);
            }
        }

        void PlayRowRemoved(Falldown manager, GameObject row)
        {
            PlayRowRemoved(row);
        }

        void PlayRowRemoved(GameObject row)
        {
            DoVibrate();
            if (null != removedRow)
            {
                player.PlayOneShot(removedRow);
            }
        }

        void PlayLevelUp(int level)
        {
            DoVibrate();
            if (null != levelUpSound)
            {
                player.PlayOneShot(levelUpSound);
            }
        }

        void PlayGameOver(GameObject manager)
        {
            if (null != gameOverSound)
            {
                player.PlayOneShot(gameOverSound);
            }
        }

        void DoVibrate()
        {
#if UNITY_IOS || UNITY_ANDROID 
                Handheld.Vibrate();
#endif
        }
    }
}