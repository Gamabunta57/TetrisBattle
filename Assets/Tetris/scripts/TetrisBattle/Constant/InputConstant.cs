﻿namespace Game.Constant
{

    public abstract class InputConstant
    {

        public const string ROTATE_LEFT = "RotateLeft";
        public const string ROTATE_RIGHT = "RotateRight";

        public const string LEFT = "Left";
        public const string RIGHT = "Right";
        public const string DOWN = "Down";
    }
}