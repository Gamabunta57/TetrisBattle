﻿namespace Game.Constant
{

    public abstract class SceneConstant
    {

        public const string PROTO_PLAY_FULL = "protoRemoveLine";
        public const string GAME_OVER = "protoGameOver";
        public const string MULTIPLAYER_SCENE = "protoMultiplayer";
    }
}