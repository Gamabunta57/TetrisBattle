﻿namespace Game.Constant
{
    public static class MessageId
    {
        static short i = 128;

        public static readonly short TEST_MESSAGE       = i++;
        public static readonly short PIECE_SPAWN        = i++;
        public static readonly short PIECE_FALL         = i++;
        public static readonly short PIECE_LEFT_RIGHT   = i++;
        public static readonly short PIECE_ROTATE       = i++;
        public static readonly short PIECE_STOP_FALL    = i++;
        public static readonly short START_GAME         = i++;
        public static readonly short ROW_REMOVE         = i++;
    }
}
