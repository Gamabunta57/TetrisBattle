﻿namespace Game.Constant
{

    public abstract class LayerConstant
    {

        public const string DEFAULT = "Default";
        public const string CURRENT_MOVING_TETRO = "CurrentMovingPiece";
        public const string ROW = "Row";
    }
}