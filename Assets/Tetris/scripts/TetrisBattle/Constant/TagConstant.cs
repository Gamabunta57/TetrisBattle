﻿
namespace Game.Constant
{
    public abstract class TagConstant
    {

        public const string MAIN_GAME_MANAGER = "Main game manager";
        public const string PLAYER = "Player";
        public const string CLIENT = "Client";
    }
}