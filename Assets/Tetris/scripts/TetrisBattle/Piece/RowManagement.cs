﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Piece
{
    public class RowManagement : MonoBehaviour
    {
        public const int MAX_BLOCK_COUNT = 10;

        public Falldown falldownComponent;

        int blockCount;
        IList<Collider> currentGoPartOnRow = new List<Collider>();

        void OnEnable()
        {
            falldownComponent.OnPieceStopFalling += OnPieceStopFalling;
        }

        void OnTriggerEnter(Collider other)
        {
            blockCount++;
            currentGoPartOnRow.Add(other);
        }

        void OnTriggerExit(Collider other)
        {
            blockCount--;
            currentGoPartOnRow.Remove(other);
        }

        void OnDisable()
        {
            falldownComponent.OnPieceStopFalling -= OnPieceStopFalling;
        }

        void OnPieceStopFalling(GameObject piece)
        {
            if (blockCount < MAX_BLOCK_COUNT)
            {
                return;
            }

            for (int i = 0; i < currentGoPartOnRow.Count; i++)
            {
                Destroy(currentGoPartOnRow[i].gameObject);
            }
            ResetRow();
            NotifyRemoveLine(falldownComponent, gameObject);
        }

        void ResetRow()
        {
            currentGoPartOnRow = new List<Collider>();
            blockCount = 0;
        }

        #region event
        public static event LineRemoved OnLineRemoved;
        public delegate void LineRemoved(Falldown manager, GameObject line);

        public static void NotifyRemoveLine(Falldown manager, GameObject line)
        {
            if (OnLineRemoved == null)
                return;

            OnLineRemoved(manager, line);
        }
        #endregion
    }
}