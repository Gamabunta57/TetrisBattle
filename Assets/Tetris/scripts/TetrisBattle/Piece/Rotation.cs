﻿using Game.Constant;
using UnityEngine;

namespace Game.Piece
{
    public class Rotation : MonoBehaviour
    {
        GameObject currentPiece;
        Collider[] colliderList;
        Vector3 piecePartSize;
        int collisionLayer;

        float[] allowedRotation;
        int currentRotationIndex;

        NewPieceGeneration npgc;

        void Awake()
        {
            npgc = GetComponent<NewPieceGeneration>();
        }

        void OnEnable()
        {
            npgc.OnPieceChange += OnPieceChange;
            piecePartSize = Vector3.one / 2 * 0.9f;

            collisionLayer = ~LayerMask.GetMask(LayerConstant.CURRENT_MOVING_TETRO, LayerConstant.ROW);
        }

        void Update()
        {
            if (null == currentPiece || allowedRotation.Length == 0)
            {
                return;
            }

            if (Input.GetButtonDown(InputConstant.ROTATE_LEFT))
            {
                TryToRotate(true);
            }
            else if (Input.GetButtonDown(InputConstant.ROTATE_RIGHT))
            {
                TryToRotate(false);
            }
        }

        void OnDisable()
        {
            npgc.OnPieceChange -= OnPieceChange;
        }

        void OnPieceChange(GameObject newPiece, Collider[] newPieceColliderList, byte pieceId)
        {
            if (null == newPiece)
            {
                return;
            }
            currentPiece = newPiece;
            colliderList = newPieceColliderList;
            allowedRotation = newPiece.GetComponent<RotationAllowed>().allowedRotation;
            currentRotationIndex = 0;
        }

        bool TryToRotate(bool isRotationToLeft)
        {
            int nextRotationIndex = isRotationToLeft ? currentRotationIndex - 1 : currentRotationIndex + 1;
            int realNextIndex = GetRotationIndex(nextRotationIndex);
            float degrees = allowedRotation[realNextIndex];
            if (!CanRotate(degrees))
            {
                return false;
            }

            currentRotationIndex = realNextIndex;
            Vector3 baseRotation = currentPiece.transform.eulerAngles;
            baseRotation.z = degrees;
            currentPiece.transform.eulerAngles = baseRotation;
            NotifyRotate(isRotationToLeft);
            return true;
        }

        int GetRotationIndex(int nextRotationIndex)
        {
            int count = allowedRotation.Length;
            if (nextRotationIndex >= count)
            {
                return 0;
            }
            if (nextRotationIndex < 0)
            {
                return count - 1;
            }
            return nextRotationIndex;
        }

        bool CanRotate(float degrees)
        {
            foreach (Collider c in colliderList)
            {
                if (CollideAfterRotation(c, degrees))
                {
                    return false;
                }
            }
            return true;
        }

        bool CollideAfterRotation(Collider c, float degrees)
        {
            Vector3 pivot = currentPiece.transform.position;
            Vector3 dir = Quaternion.AngleAxis(degrees - c.transform.parent.eulerAngles.z, Vector3.forward) * (c.transform.position - pivot);
            return Physics.OverlapBox(dir + pivot, piecePartSize, Quaternion.identity, collisionLayer).Length > 0;
        }

        #region event
        public event PieceRotate OnPieceRotate;
        public delegate void PieceRotate(bool isRotationToLeft);

        public void NotifyRotate(bool isRotationToLeft)
        {
            if (OnPieceRotate == null)
                return;

            OnPieceRotate(isRotationToLeft);
        }
        #endregion
    }
}