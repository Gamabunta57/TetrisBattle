﻿using UnityEngine;

namespace Game.Piece
{
    public class LevelHandler : MonoBehaviour
    {
        public int Level { get; private set; }
        public int rowPerLevel = 4;

        int totalRemoved;
        Falldown speedHandler;

        float baseVelocity;
        float baseFastVelocity;

        void OnEnable()
        {
            speedHandler = GetComponent<Falldown>();
            baseVelocity = speedHandler.velocity;
            baseFastVelocity = speedHandler.fastVelocity;
            RowManagement.OnLineRemoved += OnLineRemoved;
        }

        void OnDisable()
        {
            RowManagement.OnLineRemoved -= OnLineRemoved;
        }

        void OnLineRemoved(Falldown manager, GameObject piece)
        {
            if(manager != speedHandler){
                return;
            }
            totalRemoved++;
            int realLevel = (totalRemoved / rowPerLevel) + 1;
            if (realLevel > Level)
            {
                Level = realLevel;
                SetSpeed(Level);
                NotifyLevelChange(Level);
            }
        }

        void SetSpeed(int lvl)
        {
            speedHandler.velocity = (float)(baseVelocity - (baseVelocity * lvl * 0.1));
            speedHandler.fastVelocity = (float)(baseFastVelocity - (baseFastVelocity * lvl * 0.1));
        }

        #region event
        public event LevelChange OnLevelChange;
        public delegate void LevelChange(int Level);

        public void NotifyLevelChange(int Level)
        {
            if (OnLevelChange == null)
                return;

            OnLevelChange(Level);
        }
        #endregion
    }
}