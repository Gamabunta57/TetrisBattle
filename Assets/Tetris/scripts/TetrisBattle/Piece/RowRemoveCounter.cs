﻿using System.Collections;
using UnityEngine;

namespace Game.Piece
{
    public class RowRemoveCounter : MonoBehaviour
    {
        public int scorePerRow;
        public int bonusScorePerRow;
        public int Score { get; private set; }
        public int Ligne { get { return totalRemoved; } }

        int rowRemoved;
        int totalRemoved;

        void OnEnable()
        {
            RowManagement.OnLineRemoved += OnLineRemoved;
            StartCoroutine(CollectRemovedRowPerTour());
        }

        void OnDisable()
        {
            RowManagement.OnLineRemoved -= OnLineRemoved;
            StopCoroutine(CollectRemovedRowPerTour());
        }

        IEnumerator CollectRemovedRowPerTour()
        {
            while (isActiveAndEnabled)
            {
                ComputeScore();
                yield return new WaitForEndOfFrame();
            }
            yield return null;
        }

        void ComputeScore()
        {
            if (rowRemoved == 0)
            {
                return;
            }

            Score += (rowRemoved * scorePerRow) + (bonusScorePerRow * (rowRemoved - 1));
            rowRemoved = 0;
        }

        void OnLineRemoved(Falldown manager, GameObject row)
        {
            if(manager.gameObject != gameObject){
                return;
            }
            rowRemoved++;
            totalRemoved++;
        }
    }
}