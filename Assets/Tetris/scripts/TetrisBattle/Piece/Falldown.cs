﻿using UnityEngine;
using Game.Constant;

namespace Game.Piece
{
    public class Falldown : MonoBehaviour
    {
        public float velocity = 1f;
        public float fastVelocity = .1f;
        public float falldownDistance = 1f;

        float currentDelay;
        GameObject currentPiece;
        bool tryContinueFalling;
        int colliderLayerMask;

        Collider[] colliderList;

        NewPieceGeneration npgc;

        void Awake()
        {
            npgc = GetComponent<NewPieceGeneration>();
        }

        void OnEnable()
        {
            currentDelay = velocity;
            npgc.OnPieceChange += OnPieceChange;

            colliderLayerMask = ~LayerMask.GetMask(LayerConstant.CURRENT_MOVING_TETRO, LayerConstant.ROW);
        }

        void Update()
        {
            if (!tryContinueFalling || currentPiece == null)
            {
                return;
            }

            if (currentDelay > 0 && !Input.GetButtonDown(InputConstant.DOWN))
            {
                currentDelay -= Time.deltaTime;
                return;
            }

            if (!IsAbleToFalldown())
            {
                tryContinueFalling = false;
                NotifyStopFalling(currentPiece);
                return;
            }

            currentDelay = Input.GetButton(InputConstant.DOWN) ? fastVelocity : velocity;
            currentPiece.transform.position += new Vector3(0, -falldownDistance, 0);
            NotifyMoving();
        }

        void OnDisable()
        {
            npgc.OnPieceChange -= OnPieceChange;
        }

        bool IsAbleToFalldown()
        {
            foreach (Collider c in colliderList)
            {
                if (!CanFallWithCollider(c))
                {
                    return false;
                }
            }
            return true;
        }

        bool CanFallWithCollider(Collider c)
        {
            Ray r = new Ray(c.bounds.center, Vector3.down);
            Debug.DrawLine(c.bounds.center, c.bounds.center + Vector3.down * falldownDistance, Color.green, velocity);
            return !Physics.Raycast(r, falldownDistance, colliderLayerMask);
        }

        void OnPieceChange(GameObject newPiece, Collider[] newPieceColliderList,byte pieceId)
        {
            if (newPiece == null)
            {
                return;
            }
            tryContinueFalling = true;
            currentPiece = newPiece;
            colliderList = newPieceColliderList;
        }

        #region event
        public event PieceStopFalling OnPieceStopFalling;
        public delegate void PieceStopFalling(GameObject piece);

        public void NotifyStopFalling(GameObject piece)
        {
            if (OnPieceStopFalling == null)
                return;

            OnPieceStopFalling(piece);
            NotifyTurnEnd(piece);
        }

        public event PieceFalling OnPieceFalling;
        public delegate void PieceFalling();

        public void NotifyMoving()
        {
            if (null == OnPieceFalling)
            {
                return;
            }
            OnPieceFalling();
        }

        public static event TurnEnd OnTurnEnd;
        public delegate void TurnEnd(GameObject piece);

        public void NotifyTurnEnd(GameObject piece)
        {
            if (OnTurnEnd == null)
                return;

            OnTurnEnd(piece);
        }
        #endregion
    }
}