﻿using System;
using System.Collections;
using Game.Constant;
using UnityEngine;
using Game.Utils;

namespace Game.Piece
{
    public class RowFalldown : MonoBehaviour
    {

        public Transform topBlockSensorPosition;
        public Vector3 defaultSize;

        GameObject[] rowToRemoveList;
        float baseDistance;
        int hitLayer;
        int rowDeletedThisTurn;

        void OnEnable()
        {
            rowToRemoveList = new GameObject[GlobalConstant.MAX_FULL_ROW_PER_ACTION];
            RowManagement.OnLineRemoved += OnLineRemoved;
            StartCoroutine(CollectRemovedRowPerTour());
            baseDistance = GetComponent<Falldown>().falldownDistance;
            hitLayer = ~LayerMask.GetMask(LayerConstant.ROW, LayerConstant.CURRENT_MOVING_TETRO);
        }

        void OnDisable()
        {
            RowManagement.OnLineRemoved -= OnLineRemoved;
            StopCoroutine(CollectRemovedRowPerTour());
        }

        IEnumerator CollectRemovedRowPerTour()
        {
            while (isActiveAndEnabled)
            {
                if (rowDeletedThisTurn > 0)
                {
                    MakeAllRowFall();
                    rowDeletedThisTurn = 0;
                }
                yield return new WaitForEndOfFrame();
            }
            yield return null;
        }

        void MakeAllRowFall()
        {
            SortRowToRemoveByPosition();
            int[] nbRowList = new int[0];
            foreach (GameObject go in rowToRemoveList)
            {
                if(null != go){
                    MakeRowFall(go);
                    nbRowList = ArrayUtils.addElement(nbRowList, ExtractNbFromName(go));
                }
            }
            NotifyRowFall(nbRowList);
            rowToRemoveList = new GameObject[GlobalConstant.MAX_FULL_ROW_PER_ACTION];
        }

        int ExtractNbFromName(GameObject go)
        {
            return int.Parse(go.name.Substring("Row_".Length));
        }

        void SortRowToRemoveByPosition()
        {
            Array.Sort(rowToRemoveList, (a, b) => {
                if (a == b){
                    return 0;
                }
                if(a == null){
                    return 1;
                }
                if(b == null || a.transform.position.y > b.transform.position.y){
                    return -1;
                }
                return 1;
            });
        }

        void MakeRowFall(GameObject rowRemoved)
        {
            Vector3 boxSize = new Vector3(defaultSize.x, topBlockSensorPosition.position.y - rowRemoved.transform.position.y, defaultSize.z);
            Collider[] collideList = Physics.OverlapBox(topBlockSensorPosition.position, boxSize, Quaternion.identity, hitLayer);
            ExtDebug.DrawBox(topBlockSensorPosition.position,boxSize,Quaternion.identity,Color.red);
            Vector3 downSize = Vector3.down * baseDistance;

            if (collideList.Length == 0)
            {
                return;
            }

            foreach (Collider hit in collideList)
            {
                hit.gameObject.transform.position += downSize;
            }
        }

        void OnLineRemoved(Falldown manager, GameObject row)
        {
            if(manager.gameObject != gameObject){
                return;
            }
            rowToRemoveList[rowDeletedThisTurn] = row;
            rowDeletedThisTurn++;
        }

        public event RowFall OnRowFall;
        public delegate void RowFall(int[] rowList);
        public void NotifyRowFall(int[] rowList){
            if(null == OnRowFall){
                return;
            }

            OnRowFall(rowList);
        }
    }
}