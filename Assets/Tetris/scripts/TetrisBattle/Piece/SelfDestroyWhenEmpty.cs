﻿using System.Collections;
using UnityEngine;

namespace Game.Piece
{
    public class SelfDestroyWhenEmpty : MonoBehaviour
    {
        bool hasStopFalling;

        void OnEnable()
        {
            Falldown.OnTurnEnd += OnPieceStopFalling;
            StartCoroutine(SeekAndDestroy());
        }

        void OnDisable()
        {
            Falldown.OnTurnEnd -= OnPieceStopFalling;
            StopCoroutine(SeekAndDestroy());
        }

        IEnumerator SeekAndDestroy()
        {
            while (transform.childCount > 0)
            {
                yield return new WaitUntil(IsTimeToCheck);
            }
            Destroy(gameObject);
            yield return null;
        }

        void OnPieceStopFalling(GameObject piece)
        {
            hasStopFalling = true;
        }

        bool IsTimeToCheck()
        {
            bool itsTime = hasStopFalling;
            hasStopFalling = false;
            return itsTime;
        }
    }
}