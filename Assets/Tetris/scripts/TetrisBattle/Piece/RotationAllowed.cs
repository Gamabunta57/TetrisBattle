﻿using UnityEngine;

namespace Game.Piece
{
    public class RotationAllowed : MonoBehaviour
    {
        public float[] allowedRotation = { 0, 90, 180, 270 };
    }
}