﻿using UnityEngine;

namespace Game.Piece.Network
{
    public class RowRemoveCounterClient : MonoBehaviour
    {

        public int Ligne { get { return totalRemoved; } }

        int totalRemoved;

        public void AddRowRemoved(int nbRowRemoved)
        {
            totalRemoved += nbRowRemoved;
        }
    }
}
