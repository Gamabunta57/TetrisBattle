﻿using UnityEngine;

namespace Game.Piece.Network
{
    public class RotationClient : MonoBehaviour
    {

        GameObject currentPiece;
        NewPieceGenerationClient phc;
        float[] allowedRotation;
        int currentRotationIndex;

        void Awake()
        {
            phc = GetComponent<NewPieceGenerationClient>();
        }

        void OnEnable()
        {
            phc.OnPieceChange += OnPieceChange;
        }

        void OnPieceChange(GameObject newPiece)
        {
            currentPiece = newPiece;
            if (null == currentPiece)
            {
                return;
            }
            allowedRotation = newPiece.GetComponent<RotationAllowed>().allowedRotation;
            currentRotationIndex = 0;
        }

        public void MakePieceRotate(bool isRotationToLeft)
        {
            int nextRotationIndex = isRotationToLeft ? currentRotationIndex - 1 : currentRotationIndex + 1;
            int realNextIndex = GetRotationIndex(nextRotationIndex);
            float degrees = allowedRotation[realNextIndex];

            currentRotationIndex = realNextIndex;
            Vector3 baseRotation = currentPiece.transform.eulerAngles;
            baseRotation.z = degrees;
            currentPiece.transform.eulerAngles = baseRotation;
        }

        int GetRotationIndex(int nextRotationIndex)
        {
            int count = allowedRotation.Length;
            if (nextRotationIndex >= count)
            {
                return 0;
            }
            if (nextRotationIndex < 0)
            {
                return count - 1;
            }
            return nextRotationIndex;
        }
    }
}