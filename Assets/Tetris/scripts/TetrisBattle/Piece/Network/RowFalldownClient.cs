﻿using System;
using System.Collections;
using Game.Constant;
using UnityEngine;

namespace Game.Piece.Network
{

    public class RowFalldownClient : MonoBehaviour
    {

        public Transform topBlockSensorPosition;
        public Vector3 defaultSize;

        GameObject[] rowToRemoveList;
        float baseDistance;
        int hitLayer;
        int rowDeletedThisTurn;

        void OnEnable()
        {
            rowToRemoveList = new GameObject[GlobalConstant.MAX_FULL_ROW_PER_ACTION];
            RowManagementClient.OnLineRemoved += OnLineRemoved;
            StartCoroutine(CollectRemovedRowPerTour());
            baseDistance = GetComponent<FalldownClient>().falldownDistance;
            hitLayer = ~LayerMask.GetMask(LayerConstant.ROW, LayerConstant.CURRENT_MOVING_TETRO);
        }

        void OnDisable()
        {
            RowManagementClient.OnLineRemoved -= OnLineRemoved;
            StopCoroutine(CollectRemovedRowPerTour());
        }

        IEnumerator CollectRemovedRowPerTour()
        {
            while (isActiveAndEnabled)
            {
                if (rowDeletedThisTurn > 0)
                {
                    MakeAllRowFall();
                    rowDeletedThisTurn = 0;
                }
                yield return new WaitForEndOfFrame();
            }
            yield return null;
        }

        void MakeAllRowFall()
        {
            SortRowToRemoveByPosition();
            foreach (GameObject go in rowToRemoveList)
            {
                if (null != go)
                    MakeRowFall(go);
            }
            rowToRemoveList = new GameObject[GlobalConstant.MAX_FULL_ROW_PER_ACTION];
        }

        void SortRowToRemoveByPosition()
        {
            Array.Sort(rowToRemoveList, (a, b) =>
            {
                if (a == b)
                    return 0;
                if (a == null)
                    return 1;
                if (b == null || a.transform.position.y > b.transform.position.y)
                    return -1;
                return 1;
            });
        }

        void MakeRowFall(GameObject rowRemoved)
        {
            Vector3 boxSize = new Vector3(defaultSize.x, topBlockSensorPosition.position.y - rowRemoved.transform.position.y, defaultSize.z);
            Collider[] collideList = Physics.OverlapBox(topBlockSensorPosition.position, boxSize, Quaternion.identity, hitLayer);
            ExtDebug.DrawBox(topBlockSensorPosition.position, boxSize, Quaternion.identity, Color.red);
            Vector3 downSize = Vector3.down * baseDistance;
            Debug.Log("Collide list nb : " + collideList.Length + " / " + rowRemoved.name);
            if (collideList.Length == 0)
                return;

            foreach (Collider hit in collideList)
                hit.gameObject.transform.position += downSize;
        }

        void OnLineRemoved(FalldownClient manager, GameObject row)
        {
            if (manager.gameObject != gameObject)
                return;
            rowToRemoveList[rowDeletedThisTurn] = row;
            rowDeletedThisTurn++;
        }
    }
}
