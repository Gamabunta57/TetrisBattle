﻿using Game.Constant;
using UnityEngine;
using Game.Utils;

namespace Game.Piece.Network
{
    public class NewPieceGenerationClient : MonoBehaviour
    {
        public Transform startingPoint;
        public GameObject[] availablePieceList;

        GameObject currentPiece;
        int layer;

        void OnEnable()
        {
            layer = LayerMask.GetMask(LayerConstant.DEFAULT);
        }

        public void InstantiateNewPiece(byte newPieceIndex)
        {
            if (null != currentPiece)
                GameObjectUtils.SetLayerWithChildren(currentPiece, LayerMask.NameToLayer(LayerConstant.DEFAULT));
            
            currentPiece = TryInstantiateNewAvailablePiece(newPieceIndex);
            NotifyPieceChange(currentPiece);
        }

        GameObject TryInstantiateNewAvailablePiece(byte newPieceIndex)
        {
            GameObject newPiece = availablePieceList[newPieceIndex];
            if (IsRoomTaken(newPiece))
                return null;
            return Instantiate(newPiece, startingPoint.position, startingPoint.rotation);
        }

        bool IsRoomTaken(GameObject source)
        {
            foreach (BoxCollider c in source.GetComponentsInChildren<BoxCollider>())
            {
                if (IsCollidingWithExistingPiece(c))
                    return true;
            }
            return false;
        }

        bool IsCollidingWithExistingPiece(BoxCollider c)
        {
            return Physics.OverlapBox(c.transform.localPosition + startingPoint.position, c.bounds.extents, Quaternion.identity, layer).Length > 0;
        }

        public event PieceChange OnPieceChange;
        public delegate void PieceChange(GameObject newPiece);
        public void NotifyPieceChange(GameObject newPiece)
        {
            if (null == OnPieceChange)
            {
                return;
            }

            OnPieceChange(newPiece);
        }
    }
}