﻿using UnityEngine;

namespace Game.Piece.Network
{
    public class MovingLeftRightClient : MonoBehaviour
    {

        public float movingDistance = 1f;

        GameObject currentPiece; 
        NewPieceGenerationClient phc;

        void Awake()
        {
            phc = GetComponent<NewPieceGenerationClient>();
        }

        void OnEnable()
        {
            phc.OnPieceChange += OnPieceChange;
        }

        void OnPieceChange(GameObject newPiece)
        {
            currentPiece = newPiece;
        }

        public void MakePieceMoveRightOrLeft(bool isDirectionLeft)
        {
            Vector3 direction = isDirectionLeft ? Vector3.left : Vector3.right;
            currentPiece.transform.position += direction * movingDistance;
        }
    }
}