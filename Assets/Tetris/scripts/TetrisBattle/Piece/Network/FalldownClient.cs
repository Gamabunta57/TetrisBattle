﻿using UnityEngine;

namespace Game.Piece.Network
{
    public class FalldownClient : MonoBehaviour
    {

        public float falldownDistance = 1f;

        GameObject currentPiece;
        NewPieceGenerationClient phc;

        void Awake()
        {
            phc = GetComponent<NewPieceGenerationClient>();
        }

        void OnEnable(){
            phc.OnPieceChange += OnPieceChange;
        }

        void OnPieceChange(GameObject newPiece)
        {
            currentPiece = newPiece;
        }

        public void MakePieceFall(){
            currentPiece.transform.position += new Vector3(0, -falldownDistance, 0);
        }
    }
}