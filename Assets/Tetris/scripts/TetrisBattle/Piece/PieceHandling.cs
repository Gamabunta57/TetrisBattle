﻿using Game.Constant;
using UnityEngine;

namespace Game.Piece
{
    public class PieceHandling : MonoBehaviour
    {

        public GameObject currentPiece;

        Falldown falldownComponent;

        void Awake()
        {
            falldownComponent = GetComponent<Falldown>();
        }

        void OnEnable()
        {
            falldownComponent.OnPieceStopFalling += OnPieceStopFalling;
        }

        void OnDisable()
        {
            falldownComponent.OnPieceStopFalling -= OnPieceStopFalling;
        }

        void OnPieceStopFalling(GameObject piece)
        {
            Transform[] transformList = piece.GetComponentsInChildren<Transform>();
            int layer = LayerMask.NameToLayer(LayerConstant.DEFAULT);
            foreach (Transform t in transformList)
            {
                t.gameObject.layer = layer;
            }
        }

    }
}