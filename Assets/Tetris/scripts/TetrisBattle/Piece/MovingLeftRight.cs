﻿using UnityEngine;
using Game.Constant;

namespace Game.Piece
{
    public class MovingLeftRight : MonoBehaviour
    {

        public float stayDuration = 1f;
        public float velocityStay = 0.5f;
        public float movingDistance = 1f;

        float currentDelay;
        GameObject currentPiece;
        int colliderLayerMask;

        Collider[] colliderList;
        NewPieceGeneration npgc;

        void Awake()
        {
            npgc = GetComponent<NewPieceGeneration>();
        }

        void OnEnable()
        {
            currentDelay = stayDuration;
            npgc.OnPieceChange += OnPieceChange;

            colliderLayerMask = LayerMask.GetMask(LayerConstant.CURRENT_MOVING_TETRO, LayerConstant.ROW);
            colliderLayerMask = ~colliderLayerMask;
        }

        void Update()
        {
            if (currentPiece == null)
            {
                return;
            }


            if (Input.GetButtonDown(InputConstant.LEFT))
            {
                TryToMoveLeft(stayDuration);
                return;
            }

            if (Input.GetButtonDown(InputConstant.RIGHT))
            {
                TryToMoveRight(stayDuration);
                return;
            }

            bool isDelayFinished = DelayFinished();
            if (isDelayFinished && Input.GetButton(InputConstant.LEFT))
            {
                TryToMoveLeft(velocityStay);
                return;
            }

            if (isDelayFinished && Input.GetButton(InputConstant.RIGHT))
            {
                TryToMoveRight(velocityStay);
                return;
            }

            if (!isDelayFinished)
            {
                currentDelay -= Time.deltaTime;
                return;
            }
        }

        void OnDisable()
        {
            npgc.OnPieceChange -= OnPieceChange;
        }

        void TryToMoveLeft(float newDuration)
        {
            if (TryToMoveTo(Vector3.left, true))
            {
                currentDelay = newDuration;
            }
        }

        void TryToMoveRight(float newDuration)
        {
            if (TryToMoveTo(Vector3.right))
            {
                currentDelay = newDuration;
            }
        }

        bool TryToMoveTo(Vector3 direction, bool isDirectionLeft = false)
        {
            if (IsAbleToMoveInDirection(direction))
            {
                currentPiece.transform.position += direction * movingDistance;
                NotifyMoving(isDirectionLeft);
                return true;
            }
            return false;
        }

        bool DelayFinished()
        {
            return currentDelay <= 0;
        }

        bool IsAbleToMoveInDirection(Vector3 direction)
        {
            foreach (Collider c in colliderList)
            {
                if (!CanMoveInDirectionWithCollider(c, direction))
                {
                    return false;
                }
            }
            return true;
        }

        bool CanMoveInDirectionWithCollider(Collider c, Vector3 direction)
        {
            Ray r = new Ray(c.bounds.center, direction);
            Debug.DrawLine(c.bounds.center, c.bounds.center + direction * movingDistance, Color.green, stayDuration);
            return !Physics.Raycast(r, movingDistance, colliderLayerMask);
        }

        void OnPieceChange(GameObject newPiece, Collider[] newPieceColliderList, byte pieceId)
        {
            if (newPiece == null)
            {
                return;
            }
            currentPiece = newPiece;
            colliderList = newPieceColliderList;
        }

        #region event

        public event PieceMovingLeftRight OnPieceMovingLeftRight;
        public delegate void PieceMovingLeftRight(bool isDirectionLeft);

        public void NotifyMoving(bool isDirectionLeft)
        {
            if (null == OnPieceMovingLeftRight)
            {
                return;
            }
            OnPieceMovingLeftRight(isDirectionLeft);
        }
        #endregion

    }
}