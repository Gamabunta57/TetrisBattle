﻿using Game.Constant;
using Game.Utils;
using UnityEngine;

namespace Game.Piece
{
    public class NewPieceGeneration : MonoBehaviour
    {

        public GameObject[] availablePiece;
        public Transform startingPoint;

        PieceHandling handler;
        int layer;
        byte chosenPiece;

        Falldown falldownComponent;

        void Awake()
        {
            falldownComponent = GetComponent<Falldown>();
        }

        void OnEnable()
        {
            handler = GetComponent<PieceHandling>();
            layer = LayerMask.GetMask(LayerConstant.DEFAULT);
            falldownComponent.OnPieceStopFalling += OnPieceStopFalling;
        }

        void Start(){
            GenerateNewPiece();
        }

        void OnDisable()
        {
            falldownComponent.OnPieceStopFalling -= OnPieceStopFalling;
        }

        void OnPieceStopFalling(GameObject piece)
        {
            GenerateNewPiece();
        }

        void GenerateNewPiece()
        {
            handler.currentPiece = TryInstantiateNewAvailablePiece();
            NotifyPieceChanged();
        }

        GameObject TryInstantiateNewAvailablePiece()
        {
            int indexChoosen = RandomUtils.ChooseIndexFromArray(availablePiece);
            chosenPiece = (byte)indexChoosen;
            GameObject source = availablePiece[indexChoosen];
            if (IsRoomTaken(source))
            {
                NotifyEndOfGame(gameObject);
                return null;
            }
            return Instantiate(source, startingPoint.position, startingPoint.rotation);
        }

        bool IsRoomTaken(GameObject source)
        {
            foreach (BoxCollider c in source.GetComponentsInChildren<BoxCollider>())
            {
                if (IsCollidingWithExistingPiece(c))
                    return true;
            }
            return false;
        }

        bool IsCollidingWithExistingPiece(BoxCollider c)
        {
            return Physics.OverlapBox(c.transform.localPosition + startingPoint.position, c.bounds.extents, Quaternion.identity, layer).Length > 0;
        }

        #region event
        public event EndOfGame OnEndOfGame;
        public delegate void EndOfGame(GameObject manager);

        public void NotifyEndOfGame(GameObject manager)
        {
            if (null == OnEndOfGame)
            {
                return;
            }
            OnEndOfGame(manager);
        }

        public event PieceChange OnPieceChange;
        public delegate void PieceChange(GameObject newPiece, Collider[] newPieceColliderList, byte pieceId);

        public void NotifyPieceChanged()
        {
            if (OnPieceChange == null)
            {
                return;
            }
            OnPieceChange(handler.currentPiece, GameObjectUtils.GetGameObjectColliderList(handler.currentPiece), chosenPiece);
        }
        #endregion

    }
}