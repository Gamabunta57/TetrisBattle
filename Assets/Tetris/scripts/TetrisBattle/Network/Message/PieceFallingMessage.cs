﻿using UnityEngine.Networking;

namespace Game.Network.Message
{

    public class PieceFallingMessage : MessageBase
    {

        public static PieceFallingMessage FALLING{
            get {
                return new PieceFallingMessage();
            }
        }
    }
}