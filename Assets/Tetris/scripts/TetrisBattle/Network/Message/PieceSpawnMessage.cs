﻿using UnityEngine.Networking;

namespace Game.Network.Message
{

    public class PieceSpawnMessage : MessageBase
    {
        public byte PieceId;
    }
}