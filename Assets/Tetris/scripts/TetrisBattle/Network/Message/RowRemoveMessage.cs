﻿using UnityEngine.Networking;

namespace Game.Network.Message
{

    public class RowRemoveMessage : MessageBase
    {
        public int[] rowList;
    }
}