﻿using UnityEngine.Networking;

namespace Game.Network.Message
{

    public class PieceRotateMessage : MessageBase
    {
        public bool isRotationLeft;

        public static PieceRotateMessage LEFT
        {
            get
            {
                return new PieceRotateMessage { isRotationLeft = true };
            }
        }
        public static PieceRotateMessage RIGHT
        {
            get
            {
                return new PieceRotateMessage { isRotationLeft = false };
            }
        }
    }
}