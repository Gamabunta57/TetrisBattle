﻿using UnityEngine.Networking;

namespace Game.Network.Message
{

    public class PieceMoveMessage : MessageBase
    {
        public bool isDirectionLeft;

        public static PieceMoveMessage LEFT{
            get{
                return new PieceMoveMessage { isDirectionLeft = true };
            }
        }
        public static PieceMoveMessage RIGHT{
            get
            {
                return new PieceMoveMessage { isDirectionLeft = false };
            }
        }
    }
}