﻿using UnityEngine.Networking;

namespace Game.Network.Message
{

    public class StartGameMessage : MessageBase
    {

        public static StartGameMessage START
        {
            get
            {
                return new StartGameMessage();
            }
        }
    }
}