﻿using UnityEngine.Networking;

namespace Game.Network.Message
{

    public class PieceStopFallingMessage : MessageBase
    {

        public static PieceStopFallingMessage STOP_FALLING
        {
            get
            {
                return new PieceStopFallingMessage();
            }
        }
    }
}