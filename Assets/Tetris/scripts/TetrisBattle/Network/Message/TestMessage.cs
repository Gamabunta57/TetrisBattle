﻿using UnityEngine;
using UnityEngine.Networking;
using System.Text;

class TestMessage : MessageBase
{
    /*public uint netId;
    public NetworkHash128 assetId;
    public Vector3 position;
    public byte[] payload;

    // This method would be generated
    public override void Deserialize(NetworkReader reader)
    {
        netId = reader.ReadPackedUInt32();
        assetId = reader.ReadNetworkHash128();
        position = reader.ReadVector3();
        payload = reader.ReadBytesAndSize();
    }

    // This method would be generated
    public override void Serialize(NetworkWriter writer)
    {
        writer.WritePackedUInt32(netId);
        writer.Write(assetId);
        writer.Write(position);
        writer.WriteBytesFull(payload);
    }*/

    public string message;

    // This method would be generated
    public override void Deserialize(NetworkReader reader)
    {
        message = reader.ReadString();
    }

    // This method would be generated
    public override void Serialize(NetworkWriter writer)
    {
        writer.WriteBytesFull(Encoding.ASCII.GetBytes(message));
    }

}
