﻿using Game.Constant;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using Game.Network.Message;

namespace Game.Network
{
    public class Server : MonoBehaviour
    {

        public static QosType GameChannelQos = QosType.ReliableSequenced;
        public static int DefaultPort = 8888;
        public static int MaxConnectionAllowed = 10;

        bool isStarted;
        Dictionary<int, NetworkConnection> connectedClient = new Dictionary<int, NetworkConnection>();

        public void StartServer()
        {
            StartServer(DefaultPort);
        }

        public void StartServer(int port)
        {
            DontDestroyOnLoad(this);
            DefaultPort = port;
            isStarted = NetworkServer.Configure(GetTopology()) && NetworkServer.Listen(port);
            RegisterAllHandlers();
        }

        public static HostTopology GetTopology()
        {
            ConnectionConfig config = new ConnectionConfig();
            config.AddChannel(GameChannelQos);
            return new HostTopology(config, MaxConnectionAllowed);
        }

        void RegisterAllHandlers()
        {
            NetworkServer.RegisterHandler(MsgType.Connect, OnConnect);
            NetworkServer.RegisterHandler(MsgType.Disconnect, OnDisconnect);
            NetworkServer.RegisterHandler(MessageId.PIECE_FALL, OnPieceFall);
            NetworkServer.RegisterHandler(MessageId.PIECE_LEFT_RIGHT, OnPieceLeftRight);
            NetworkServer.RegisterHandler(MessageId.PIECE_ROTATE, OnPieceRotate);
            NetworkServer.RegisterHandler(MessageId.PIECE_STOP_FALL, OnPieceStopFall);
            NetworkServer.RegisterHandler(MessageId.PIECE_SPAWN, OnPieceSpawn);
        }

        void OnPieceFall(NetworkMessage netMsg)
        {
            TransmitToAllOtherConnection(MessageId.PIECE_FALL, netMsg.ReadMessage<PieceFallingMessage>(), netMsg.conn.connectionId);
            Debug.Log("Fall");
        }

        void OnPieceLeftRight(NetworkMessage netMsg)
        {
            TransmitToAllOtherConnection(MessageId.PIECE_LEFT_RIGHT, netMsg.ReadMessage<PieceMoveMessage>(), netMsg.conn.connectionId);
            Debug.Log("Left / Right : " + netMsg);
        }

        void OnPieceRotate(NetworkMessage netMsg)
        {
            TransmitToAllOtherConnection(MessageId.PIECE_ROTATE, netMsg.ReadMessage<PieceRotateMessage>(), netMsg.conn.connectionId);
            Debug.Log("Rotate : " + netMsg);
        }

        void OnPieceStopFall(NetworkMessage netMsg)
        {
            TransmitToAllOtherConnection(MessageId.PIECE_STOP_FALL, netMsg.ReadMessage<PieceStopFallingMessage>(), netMsg.conn.connectionId);
            Debug.Log("StopFall : " + netMsg);
        }

        void OnPieceSpawn(NetworkMessage netMsg)
        {
            TransmitToAllOtherConnection(MessageId.PIECE_SPAWN, netMsg.ReadMessage<PieceSpawnMessage>(), netMsg.conn.connectionId);
            Debug.Log("PieceSpawn : " + netMsg);
        }

        void OnConnect(NetworkMessage netMsg)
        {
            connectedClient.Add(netMsg.conn.connectionId, netMsg.conn);
            Debug.Log("Connect : " + netMsg.conn.connectionId);

            if (connectedClient.Count > 1)
            {
                SendToAll(MessageId.START_GAME, StartGameMessage.START);
            }
        }

        void OnDisconnect(NetworkMessage netMsg)
        {
            Debug.Log("Disonnect : " + netMsg);
        }

        void SendToAll(short messageType, MessageBase message)
        {
            foreach (KeyValuePair<int, NetworkConnection> connection in connectedClient)
                connection.Value.Send(messageType, message);
        }

        void TransmitToAllOtherConnection(short messageType, MessageBase message, int senderId)
        {
            foreach (KeyValuePair<int, NetworkConnection> connection in connectedClient)
            {
                if (connection.Key != senderId)
                    connection.Value.Send(messageType, message);
            }
        }
    }
}