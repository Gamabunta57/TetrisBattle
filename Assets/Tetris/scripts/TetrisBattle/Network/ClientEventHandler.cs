﻿using UnityEngine;
using Game.Piece;
using UnityEngine.Networking;
using Game.Constant;
using Game.Network.Message;
using System;

public class ClientEventHandler : MonoBehaviour
{

    public GameObject manager;
    Falldown fc;
    MovingLeftRight mlrc;
    Rotation rc;
    NewPieceGeneration npgc;
    NetworkClient client;

    // Use this for initialization
    void Start()
    {
        fc = manager.GetComponent<Falldown>();
        mlrc = manager.GetComponent<MovingLeftRight>();
        rc = manager.GetComponent<Rotation>();
        npgc = manager.GetComponent<NewPieceGeneration>();

        fc.OnPieceFalling += OnPieceFalling;
        fc.OnPieceStopFalling += OnPieceStopFalling;
        mlrc.OnPieceMovingLeftRight += OnPieceMovingLeftRight;
        rc.OnPieceRotate += OnPieceRotate;
        npgc.OnPieceChange += OnPieceChange;

        client = NetworkUtils.GetClient();
    }
    void OnPieceFalling()
    {
        client.Send(MessageId.PIECE_FALL, PieceFallingMessage.FALLING);
        Debug.Log("SEND PIECE_FALL");
    }

    void OnPieceStopFalling(GameObject piece)
    {
        client.Send(MessageId.PIECE_STOP_FALL, PieceStopFallingMessage.STOP_FALLING);
        Debug.Log("SEND PIECE_STOP_FALL");
    }

    void OnPieceMovingLeftRight(bool isDirectionLeft)
    {
        client.Send(MessageId.PIECE_LEFT_RIGHT, isDirectionLeft ? PieceMoveMessage.LEFT : PieceMoveMessage.RIGHT);
        Debug.Log("SEND MOVING : " + (isDirectionLeft ? "LEFT" : "RIGHT"));
    }

    void OnPieceRotate(bool isRotationLeft)
    {
        client.Send(MessageId.PIECE_ROTATE, isRotationLeft ? PieceRotateMessage.LEFT : PieceRotateMessage.RIGHT);
        Debug.Log("SEND ROTATING : " + (isRotationLeft ? "LEFT" : "RIGHT"));
    }

    void OnPieceChange(GameObject newPiece, Collider[] newPieceColliderList, byte pieceId)
    {
        client.Send(MessageId.PIECE_SPAWN, new PieceSpawnMessage { PieceId = pieceId });
        Debug.Log("SEND NEW PIECE : " + pieceId.ToString());
    }

}
