﻿using UnityEngine;
using UnityEngine.Networking;

namespace Game.Network
{
    public class Client : MonoBehaviour
    {
        
        bool IsLocalClient
        {
            get
            {
                return ServerIp == "127.0.0.1" || ServerIp == "localhost";
            }
        }

        public string ServerIp = "127.0.0.1";
        public int DefaultPort = 8888;
        public bool ForceNetworkClient = true;

        int GameChannelId;
        int hostId;
        int connectionId;
        int bufferSize = 128;
        bool isStarted;
        ConnectionConfig connectionConfiguration;
        NetworkClient client;

        public void DoStart()
        {
            if(!ForceNetworkClient && IsLocalClient){
                StartLocalClient();
            }else{
                StartClient();
            }

            NetworkUtils.InitClient(client);
        }

        public void StartLocalClient()
        {
            DontDestroyOnLoad(this);
            client = ClientScene.ConnectLocalServer();
            RegisterAllHandler();
            client.RegisterHandler(MsgType.Disconnect, OnDisconnected);
        }

        public void StartClient()
        {
            StartClient(ServerIp,DefaultPort);
        }

        public void StartClient(string ip, int port){
            DontDestroyOnLoad(this);
            ServerIp = ip;
            DefaultPort = port;

            client = new NetworkClient();
            client.Configure(Server.GetTopology());
            RegisterAllHandler();
            client.Connect(ip, port);
        }

        public NetworkClient GetNetworkClient(){
            return client;
        }

        void RegisterAllHandler()
        {
            client.RegisterHandler(MsgType.Connect, OnConnected);
        }

        void OnConnected(NetworkMessage netMsg)
        {
            Debug.Log("Connected on server ");
        }

        void OnDisconnected(NetworkMessage netMsg)
        {
            Debug.Log("Disconnected from server");
        }

        public void RegisterHandler(short connect, NetworkMessageDelegate connectionCallback)
        {
            client.RegisterHandler(connect, connectionCallback);
        }
    }
}