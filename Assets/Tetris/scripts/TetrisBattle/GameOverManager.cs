﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Game.Constant;

namespace Game
{
    public class GameOverManager : MonoBehaviour
    {

        public GameObject scoreTarget;
        public GameObject levelTarget;
        public GameObject ligneTarget;

        GameObject scoreSource;
        GameObject levelSource;
        GameObject ligneSource;

        void OnEnable()
        {
            scoreSource = GameObject.Find(scoreTarget.name.Replace("_", "$"));
            levelSource = GameObject.Find(levelTarget.name.Replace("_", "$"));
            ligneSource = GameObject.Find(ligneTarget.name.Replace("_", "$"));

            scoreTarget.GetComponent<Text>().text = scoreSource.GetComponent<Text>().text;
            levelTarget.GetComponent<Text>().text = levelSource.GetComponent<Text>().text;
            ligneTarget.GetComponent<Text>().text = ligneSource.GetComponent<Text>().text;
        }

        public void Replay()
        {
            SceneManager.LoadScene(SceneConstant.PROTO_PLAY_FULL);
        }

        public void Quit()
        {
            GameGlobal.QuitGame();
        }

    }
}