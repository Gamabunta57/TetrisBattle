﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Game.Constant;

public class GameGlobal : MonoBehaviour
{

    public static void QuitGame()
    {
        Application.Quit();
    }

    public void RunMainScene(){
        SceneManager.LoadScene(SceneConstant.PROTO_PLAY_FULL);
    }
}
