﻿using Game.Network;
using UnityEngine;
using UnityEngine.Networking;
using Game.Constant;

public static class NetworkUtils
{
    static NetworkClient client;
    static bool isClientStarted;

    public static NetworkClient GetClient()
    {
        if(null == client){
            client = GameObject.FindWithTag(TagConstant.CLIENT).GetComponent<Client>().GetNetworkClient();
        }
        return client;
    }

    public static void InitClient(NetworkClient client)
    {
        NetworkUtils.client = client;
    }

    public static void StartClient(string ip, int port, NetworkMessageDelegate ConnectionCallback)
    {
        GameObject clientManager = new GameObject("Client network");
        clientManager.tag = TagConstant.CLIENT;
        Client cc = clientManager.AddComponent<Client>();
        cc.StartClient(ip, port);
        cc.RegisterHandler(MsgType.Connect, ConnectionCallback);
    }

    public static void StartHost(int port, NetworkMessageDelegate ConnectionCallback)
    {
        GameObject clientManager = new GameObject("Host network");
        clientManager.tag = TagConstant.CLIENT;

        Server sc = clientManager.AddComponent<Server>();
        Client cc = clientManager.AddComponent<Client>();

        sc.StartServer(port);
        cc.StartLocalClient();
        cc.RegisterHandler(MsgType.Connect, ConnectionCallback);
    }

    public static void StartServer(int port)
    {
        GameObject clientManager = new GameObject("Server network");
        Server sc = clientManager.AddComponent<Server>();
        sc.StartServer(port);
    }
}
