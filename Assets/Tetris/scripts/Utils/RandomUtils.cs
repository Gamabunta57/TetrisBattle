﻿using System;
using Random = UnityEngine.Random;

namespace Game.Utils
{

    public abstract class RandomUtils
    {

        static int randomSeed;

        public static void InitSeedWith(int seed)
        {
            randomSeed = seed;
            Random.InitState(seed);
        }

        public static int GetCurrentSeed()
        {
            return randomSeed;
        }

        public static int InitRandom()
        {
            int seed = (int)DateTime.Now.Ticks;
            InitSeedWith(seed);
            return seed;
        }

        public static Int32 GetBetween(Int32 min, Int32 max)
        {
            return Random.Range(min, max);
        }

        public static Int32 ChooseIndexFromArray<T>(T[] list)
        {
            Int32 start = list.GetLowerBound(0);
            Int32 end = list.GetUpperBound(0);
            return GetBetween(start, end + 1);
        }
    }
}