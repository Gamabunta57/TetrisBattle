﻿using System.Collections.Generic;
using System;
using UnityEngine;

namespace Game.Utils{
	
	public abstract class ArrayUtils {

		public static T[] addElement<T>(T[] list, T element){
			if (null == list)
				return ArrayUtils.initNewList<T> (element);
				
			T[] new_list = ArrayUtils.initNewListFromBaseList (list);
			new_list.SetValue (element, new_list.Length - 1);

			return new_list;
		}

		public static T getAndRemoveAt<T>(ref T[] list, Int32 index){
			if(null == list)
				throw new ArgumentException("Retirer un élément d'un tableau vide n'est pas possible");
			
			T element = list[index];
			T[] dest = new T[list.Length - 1];
			
			if( index > 0 )
				Array.Copy(list, 0, dest, 0, index);

			if( index < list.Length - 1 )
				Array.Copy(list, index + 1, dest, index, list.Length - index - 1);

			list = dest;

			return element;
		}

		public static bool contains<T>(T[] list, T elementToSearchFor){
			if(null == list)
				return false;
			return Array.IndexOf<T>(list, elementToSearchFor) != -1;
		}

		public static bool containsMatching<T>(T[] list, Predicate<T> filter){
			return ArrayUtils.filter(list, filter).Length > 0;
		}

		public static T[] filter<T>(T[] list, Predicate<T> filter){
			return Array.FindAll(list, filter);
		}

		public static T getFirst<T>(T[] list, Predicate<T> filter){
			T element = ArrayUtils.tryGetFirst<T>(list,filter);
			if(null == element)
				throw new ArgumentException("Il n'y a pas d'élément correspondant au filtre");
			return element;
		}

		public static T tryGetFirst<T>(T[] list, Predicate<T> filter){
			T[] element_list = ArrayUtils.filter<T>(list, filter);
				if(element_list.Length > 0)
					return element_list[0];
			
			return default(T);
		}

		public static void sort<T>(ref T[] list, Comparison<T> sortMethod){
			Array.Sort(list, sortMethod);
		}

		public static void each<T>(ref T[] list, Action<T> actionToDo){
			Array.ForEach<T>(list,actionToDo);
		}

		public static void walk<T>(T[] list, Action<T> actionToDo){
			Array.ForEach<T>(list,actionToDo);
		}

		private static T[] initNewListFromBaseList<T>(T[] base_list){
			T[] list = new T[base_list.Length + 1];
			base_list.CopyTo (list,0);
			return list;
		}

		private static T[] initNewList<T>(T element){
			T[] list = new T[1];
			if (null != element)
				list.SetValue (element, 0);
			return list;
		}
	}
}