﻿using System;
using UnityEngine;

namespace Game.Utils
{
    public abstract class GameObjectUtils
    {
        public static Collider[] GetGameObjectColliderList(GameObject go)
        {
            if (null == go)
            {
                return null;
            }
            return go.transform.GetComponentsInChildren<Collider>();
        }

        public static void SetLayerWithChildren(GameObject currentPiece, int layer)
        {
            foreach (Transform t in currentPiece.GetComponentsInChildren<Transform>())
                t.gameObject.layer = layer;
        }
    }
}