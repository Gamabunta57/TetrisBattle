﻿using Game.Piece;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    public class LevelReader : MonoBehaviour
    {

        public LevelHandler levelHandlerComponent;

        Text text;

        void Awake()
        {
            text = GetComponent<Text>();
        }

        void OnEnable()
        {
            text = GetComponent<Text>();
            levelHandlerComponent.OnLevelChange += OnLevelChange;
        }

        void OnDisable()
        {
            levelHandlerComponent.OnLevelChange -= OnLevelChange;
        }

        void OnLevelChange(int Level)
        {
            text.text = Level.ToString();
        }
    }
}
