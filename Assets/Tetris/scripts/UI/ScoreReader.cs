﻿using Game.Piece;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    public class ScoreReader : MonoBehaviour
    {

        public RowRemoveCounter scoreHandler;

        Text text;

        void OnEnable()
        {
            text = GetComponent<Text>();
        }

        void LateUpdate()
        {
            text.text = scoreHandler.Score.ToString();
        }
    }
}