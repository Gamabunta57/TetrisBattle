﻿using Game.Piece;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    public class LigneReader : MonoBehaviour
    {

        public RowRemoveCounter ligneHandler;

        Text text;

        void OnEnable()
        {
            text = GetComponent<Text>();
        }

        void LateUpdate()
        {
            text.text = ligneHandler.Ligne.ToString();
        }
    }
}