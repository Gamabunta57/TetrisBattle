﻿using UnityEngine;

public class ServerLauncher : MonoBehaviour {

    public uint Port = 8888;

	void Awake () {
        NetworkUtils.StartServer((int)Port);
	}
}
