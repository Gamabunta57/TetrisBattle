﻿using UnityEngine.Networking;

public class WaitFor2TeamConnected : NetworkManager
{

    public int NumberOfTeamToWaitFor = 2;
    int clientConnectedCount;

    public override void OnClientConnect(NetworkConnection conn)
    {
        base.OnClientConnect(conn);
        clientConnectedCount++;
        if (clientConnectedCount >= NumberOfTeamToWaitFor)
        {
            NotifyEnoughClientConnected();
        }
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);
        clientConnectedCount--;
        if (clientConnectedCount < NumberOfTeamToWaitFor)
        {
            NotifyNotEnoughClientConnected();
        }
    }

    #region event
    public static event EnoughClientConnected OnEnoughClientConnected;
    public delegate void EnoughClientConnected();

    public static void NotifyEnoughClientConnected(){
        if(null == OnEnoughClientConnected){
            return;
        }

        OnEnoughClientConnected();
    }

    public static event NotEnoughClientConnected OnNotEnoughClientConnected;
    public delegate void NotEnoughClientConnected();

    public static void NotifyNotEnoughClientConnected()
    {
        if (null == OnNotEnoughClientConnected)
        {
            return;
        }

        OnNotEnoughClientConnected();
    }
    #endregion
}
